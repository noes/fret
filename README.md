## Instructions:
 
cd to folder with the fret script. 
Run 'php fret -h' command for help about how to use it.

If on Linux, you can also place the script in one the directories in the $PATH variable and run it by typing 'fret -h' without the 'php' at the beginning.